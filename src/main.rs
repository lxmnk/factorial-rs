#![feature(exclusive_range_pattern)]
use std::env;

fn factorial(n: u64) -> u64 {
    match n {
        0..2 => 1,
        _ => {
            let mut temp = 1;
            for i in 2..(n + 1) {
                temp *= i;
            }
            temp
        }
    }
}

#[test]
fn test_factorial() {
    assert_eq!(factorial(0), 1);
    assert_eq!(factorial(1), 1);
    assert_eq!(factorial(2), 2);
    assert_eq!(factorial(5), 120);
    assert_eq!(factorial(16), 20922789888000);
}

fn main() {
    if env::args().skip(1).len() != 1 {
        println!("Usage: factorial NUMBER");
        std::process::exit(1);
    }
    let arg: String = env::args().skip(1).collect();
    let n: u64 = arg
        .parse()
        .expect(&format!("{} is not an integer number!", arg));
    println!("{}", factorial(n));
}
